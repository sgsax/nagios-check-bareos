# check_bareos.p[ly]

Modified version of [check_bacula.pl](https://exchange.nagios.org/directory/Plugins/Backup-and-Recovery/Bacula/check_bacula-2Epl/details) that queries a Bareos PostgreSQL catalog.

Use it exactly as you would check_bacula.pl:

```
Usage: check_bareos.pl -H <hours> -c <critical> -w <warning> -j <job-name> [ -e <errors> ] [ -b ] [ -a ] [ -r ] [ -h ] [ -V ]

If Bareos holds its data behind password, you have to manually enter the password into the script as variable $sqlPassword.
And be sure to prevent everybody from reading it!

Options:
H   check successful jobs within <hours> period
c   number of successful jobs for not returning critical
w   number of successful jobs for not returning warning
e   number of jobs' errors for not returning warning
b   check ClientRunBeforeJob error status and return warning in the case
a   check ClientRunAfterJob error status and return warning in the case
r   in case of 1 required job issue warning if the job is still running
j   name of the job to check (case-sensitive)
h   show this help
V   print script version
```
