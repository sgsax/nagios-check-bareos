#!/usr/bin/python3

import argparse
from datetime import datetime, timedelta
import psycopg2
import re
import sys

dbuser = "bareos"       # your Bareos database user
db = "bareos"           # your Bareos database name
dbpass = "password"     # your Bareos database password

error_count = 0
count = 0
jobid = 0
joblog = ""

ERRORS = {"UNKNOWN": -1, "OK": 0, "WARNING": 1, "CRITICAL": 2}

parser = argparse.ArgumentParser(description="Check status of Bareos backup jobs")
parser.add_argument("-H", "--hours", help="check successful jobs within <hours> period", action="store", type=int, required=True)
parser.add_argument("-c", "--critical", help="number of successful jobs for not returning critical", action="store", type=int, required=True)
parser.add_argument("-w", "--warning", help="number of successful jobs for not returning warning", action="store", type=int, required=True)
parser.add_argument("-e", "--errors", help="number of jobs' errors for not returning warning", action="store", type=int)
parser.add_argument("-b", "--beforescript", help="check ClientRunBeforeJob error status and return warning in the case", action="store_true")
parser.add_argument("-a", "--afterscript", help="check ClientRunAfterJob error status and return warning in the case", action="store_true")
parser.add_argument("-r", "--running", help="in case of 1 required job issue warning if the job is still running", action="store_true")
parser.add_argument("-j", "--job", help="name of the job to check (case-sensitive)", action="store", required=True)
parser.add_argument("-V", "--version", help="print script version", action="store_true")
args = parser.parse_args()

now = datetime.today()
date_stop = datetime.fromtimestamp(0)
date_start = now.replace(microsecond=0)

if args.hours > 0:
    date_stop = now - timedelta(hours=args.hours)

sql = "SELECT SUM(JobErrors) AS errors, COUNT(*) AS count, Job.JobId, Job.JobStatus, Log.LogText FROM Job LEFT JOIN Log on Job.JobId = Log.JobId WHERE (Name=%s) AND (JobStatus='T') AND (EndTime IS NOT NULL) AND ((EndTime <= %s) AND (EndTime >= %s)) GROUP BY Job.JobId, Job.JobStatus, Log.LogText;"

sqlvars = (args.job, date_start.isoformat(sep=' '), date_stop.isoformat(sep=' '), )

conn = psycopg2.connect("dbname='"+db+"' user='"+dbuser+"' password='"+dbpass+"' host='localhost'")
cursor = conn.cursor()
cursor.execute(sql, sqlvars)
data = cursor.fetchone()
cursor.close()

if data:
    error_count = data[0]
    count = data[1]
    jobid = data[2]
    joblog = data[4]

else:
    print("Bareos UNKNOWN: No data found")
    conn.close()
    sys.exit(ERRORS["UNKNOWN"])

state = "OK"
msg = ""

if args.errors:
    if error_count > args.errors:
        state = "WARNING"
    msg += ", " + error_count + " job errors"

if args.beforescript and (joblog is not "") and re.match('BeforeJob returned non-zero status', joblog):
    state = "WARNING"
    msg += ", Runscript BeforeJob of job " + str(jobid) + " exited abnormally"


if args.afterscript and (joblog is not "") and re.match('AfterJob returned non-zero status', joblog):
    state = "WARNING"
    msg += ", Runscript AfterJob of job " + str(jobid) + " exited abnormally"

if count < args.warning:
    state = "WARNING"

if count < args.critical:
    if running and (critical == 1):
        data = None
        sql = "SELECT COUNT(*) AS count FROM Job WHERE (Name=%s) AND (JobStatus='R');"
        sqlvars = (args.job, )
        cursor = conn.cursor()
        cursor.execute(sql, sqlvars)
        data = cursor.fetchall()
        cursor.close()
       
        if data > 0:
            state = "WARNING"
            msg += ", job is still running"
        else:
            state = "CRITICAL"
    else:
        state = "CRITICAL"

print("Bareos " + state + ": Found " + str(count) + " successful jobs" + msg)
conn.close()
sys.exit(ERRORS[state])

